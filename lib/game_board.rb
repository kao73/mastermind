require 'colorize'

class GameBoard
  attr_reader :board
  def initialize
    @board = genBoard
  end
  
  private
  def genBoard
    b = []
    4.times{
      num = 0
      loop do
        num = rand(10) # generate a number in range 0-9
        break unless b.include? num # numbers must be unique
      end
      b.push(num)
    }
    b.join().to_s
  end
  
  # check the guess value  
  public
  def guess(value)
    verify(value)

    res = { :bull => 0, :cow => 0 }
    for i in 0..3  
      if value[i].eql? @board[i]
        res[:bull] += 1
      else 
        res[:cow] += 1 if @board.include? value[i]
      end
    end
    return res
  end

  def verify(guess)
    raise ArgumentError, :ERR_NOT_A_STRING unless guess.is_a? String
    raise ArgumentError, :ERR_NOT_4_SYMBOLS unless guess.length == 4
    raise ArgumentError, :ERR_NOT_A_DIGIT unless /^\d{4}$/ === guess
    raise ArgumentError, :ERR_NOT_UNIQUE unless guess.split("").uniq.size == 4 
  end  
end

class GameApp
  def initialize
    @board = GameBoard.new
  end

  def start
    main_process
  end
    
  def main_process
    # puts "#{@board.board}"
    puts "I proposed a four-digints number. Can you guess it, please?".green
    puts "Please note, every single attempt must be just four-digits string!".light_red
    puts "Type 'exit' if you ready to give up.".light_red
    puts "Type 'help' if you don't remember the rules.".light_red
    begin # while
      begin # try/catch
        print "Guess?: ".light_yellow
        guess = gets.chomp
        if guess.downcase.eql? "exit"
          puts "That was #{@board.board}\nBad luck".light_blue
          abort
        elsif guess.downcase.eql? "help"
          puts "You guess my number by entering yours. There are four places in the number."
          puts "If one of your digits also present in mine - it's a " + "Cow".light_red
          puts "If you guessed even the position of the digit - it's a " + "Bull".light_green
          puts "So easy. Huh!?"
          puts
          next
        end
        res = @board.guess(guess)
        print "Bulls: #{res[:bull]} ".light_green
        print "Cows: #{res[:cow]}".light_red
        puts
      rescue ArgumentError => e
        puts case e.message.to_sym
          when :ERR_NOT_A_STRING
            "Please enter strings only" # should never happen
          when :ERR_NOT_4_SYMBOLS
            "Please enter four digits only"
          when :ERR_NOT_A_DIGIT
            "Please enter digits only"
          when :ERR_NOT_UNIQUE
            "All digints must be unique"
          else 
            "Something goes wrong: #{e.message}"
        end
      end 
    end while res.nil? or res[:bull] < 4
    puts "You are perfect!".green.underline
  end

end

if __FILE__ == $0
  GameApp.new.start
end
