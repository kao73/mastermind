# Mister Mind #

This is a simplest implementation of the MasterMind game also known as [Bulls and Cows](https://en.wikipedia.org/wiki/Bulls_and_Cows)

I use the project to study Ruby language

### How do I get set up? ###

Just execute the following command line
```
ruby ./lib/game_board.rb
```

### Who do I talk to? ###

* Repo owner: Alexey Kolesnikov <kao@simbirsoft.com>
