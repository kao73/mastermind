require 'simplecov'
SimpleCov.start

require "game_board.rb"
require "./config/constants.rb"

RSpec.configure do |c|
  c.include GlobalConstants
end

describe GameBoard do

  before :all do
    @board = GameBoard.new
  end
  
  context "creating" do

    describe "#initialize" do
      it "should have board initialized" do
        expect(@board.board).not_to be_empty
      end

      it "should have 4 characters" do
        expect(@board.board.length).to be(4)
      end

      it "should have only numbers" do
        @board.board.split("").each { |c| 
          expect(c.to_i).to be >=0 
          expect(c.to_i).to be <=9 
        }
      end
      
      it "should have unique numbers" do
        @board.board.split("").each { |c|
          expect(@board.board.scan(c).size).to be(1)
        }
      end
    end
  end

  context "verifying" do
    describe "#verify" do

      it "should take only string parameter" do
        expect { @board.verify(1234) }.to raise_error(ArgumentError, :ERR_NOT_A_STRING.to_s)
        expect { @board.verify("1234") }.not_to raise_error
      end
      
      it "should take 4 symbols in parameter" do
        expect { @board.verify("1") }.to raise_error(ArgumentError, :ERR_NOT_4_SYMBOLS.to_s)
        expect { @board.verify("1234") }.not_to raise_error
      end

      it "should take only digits in parameter" do
        expect { @board.verify("a123") }.to raise_error(ArgumentError, :ERR_NOT_A_DIGIT.to_s)
        expect { @board.verify("1234") }.not_to raise_error
      end
      
      it "should take unique digits in parameter" do
        expect { @board.verify("1123") }.to raise_error(ArgumentError, :ERR_NOT_UNIQUE.to_s)
        expect { @board.verify("1234") }.not_to raise_error
      end

    end
  end
  
  context "guessing" do
    describe "#guess" do
      
      it "should return hash with 'bull' and 'cow'" do
        res = @board.guess(@board.board)
        expect(res).to include(:bull)
        expect(res).to include(:cow)
      end

      it "should return empty result" do
        val = []
        4.times{
          num = 0
          loop do
            num = rand(10)
            break unless @board.board.include? num.to_s or val.include? num
          end
          val.push(num)
        }
        val = val.join.to_s
        res = @board.guess(val)
        expect(res).to include( :bull => 0, :cow => 0)
      end
      
      it "should return 4 cows and 0 bulls" do
        expect(@board.guess(@board.board.reverse)).to include(:bull => 0, :cow => 4)
      end
      
      it "should return 0 cows and 4 bulls" do
        expect(@board.guess(@board.board)).to include(:bull => 4, :cow => 0)
      end
      
    end
  end

end
